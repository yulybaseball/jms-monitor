#!/bin/env python

"""
This script connects to JMS servers and gets the information of the queues.

Usage:

./jmsmonitor.py [carrier] [-j <jms>] [-p <purpose>] [-s <sorting>]

carrier: carrier to check the queues from. If left blank, this script will 
    check the queues for all carriers. Name of the carrier to pass in here 
    should match the key representing the name of the carrier in the conf
    file.
jms: name of the JMS to check the queues from. If this value and carrier name 
    are both provided, this script will check whether the conf file has 
    defined the provided JMS under the list of the provided carrier.
purpose: either web or alert. If web is specified, this script will print 
    the output in JSON format, acting as an API for web requests. If alert 
    is specified, thresholds defined in the conf file will be analized and 
    an alert email will be send if neecessary. If this option is left blank, 
    this script will print to the output as regular text.
sorting: either count or name. Output will be sorted out by the amount of 
    messages in the queues (column 'Current Messages'), followed by the 
    amount of messages in pending queue ('Pending Messages'), 
    or by the name of the queues, which is the default sorting mode (so it's 
    not necessary specify this option if sorting by name).

Please note this script depends on /rmt/_jms_monitor_rmt.py, which is used as 
template to create dynamicaly the scripts to be executed into the JMSs.

Configuration file to be used by this script has this form:
{
    "settings": {
        "email": {
            "dest": [<users2send_email_to],
            "subject": <email_subject>,
            "high_importance": true | false,
            "body": <body_of_the_email>
        },
        "defaults": {
            "threshold": <threshold_for_triggering_alerts>,
            "username": <username2connect2jms>,
            "port": <port2connect2jms>,
            "wlst_command": <java_command2call_wlst>,
            "wlst_script": <set_env_command>,
            "wlst_queue_bean": <specific_string_for_jms_queues>,
            "normal_queue": true | false
        }
        "carriers": {
            <carrier_name>: [<list_of_JMSs>],
            ...
        },
        "jmss": {
            <jms_name>: {
                "threshold": <threshold_for_triggering_alerts>,
                "username": <username2connect2jms>,
                "port": <port2connect2jms>,
                "wlst_script": <set_env_command>,
                "wlst_queue_bean": <specific_string_for_jms_queues>,
                "normal_queue": true | false
                "q_thresholds": {
                    <queue_name>: <threshold2trigger_alert_for_this_queue>,
                    ...
                }
            },
            ...
        }
    }
}
Every key under <jms_name> is optional, in which case this script will use the 
value specified under "defaults".

All options/arguments passed to this script are case sensitive.
"""

import ast
import getopt
from threading import Lock
from os import path, system
import subprocess
import sys
import time

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from pssapi.logger import logger as logmod
from pssapi.utils import conf, util
from pssapi.emailer import emailer

from multiprocessing.pool import ThreadPool as Pool

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'jmsmonitor.log')
CONF_FILE = path.join(EXEC_DIR, 'conf', 'conf.json')
CREDENTIALS_FILE = path.join(path.dirname(EXEC_DIR), 'sec', 'jms.json')
REMOTE_EXE_FILE = path.join(EXEC_DIR, 'rmt', '_jms_monitor_rmt.py')
REMOTE_EXE_FILE_TEMP = path.join(EXEC_DIR, 'rmt', 
                                '{jms_name}_{current_time}_jms_monitor_rmt.py')

OPTIONS = "j:p:q:s:"
# possible values to the options passed to this script
POSS_VALUES = {
    '-p': ['web', 'alert'],
    '-s': ['count', 'name']
}
COMMAND = ("scp {script} {user}@{jms}:; ssh {user}@{jms} \". " + 
           "{set_env_script} > /dev/null 2>&1; java weblogic.WLST " + 
           "-skipWLSModuleScanning {rmt_script} > /dev/null 2>&1; " + 
           "cat {json_result}; rm -f {rmt_script} {json_result}\"")
logger = None
jms_data = None
s_action_lock = None

def _init(params):
    """Initialize global objects."""
    global jms_data
    global s_action_lock
    global logger
    jms_data = {}
    s_action_lock = Lock()
    logger_fmtter = {
        'INFO': '%(asctime)s - %(process)d - %(message)s',
        'DEFAULT': '%(asctime)s - %(process)d - %(levelname)s - %(message)s'
    }
    logger = logmod.load_logger(log_file_name=LOG_FILE, cgi=True, **logger_fmtter)
    logger.info("I was called this way: {0}".format(" ".join(params)))

def _check_params(params):
    """Check only one carrier can be specified in the parameters, and check 
    the rest of the options are pre-defined in POSS_VALUES.
    """
    parameters = params[1:] if len(params) > 1 else []
    arguments = {}
    opts, carrier = getopt.gnu_getopt(parameters, OPTIONS)
    if len(carrier) > 1:
        raise Exception("Maximum one carrier can be specified.")
    arguments['carrier'] = carrier[0] if carrier else ''
    for opt in opts:
        opt_name = opt[0]
        opt_value = opt[1]
        if opt_name in POSS_VALUES and \
            opt_value not in POSS_VALUES[opt_name]:
            error = ("Option {0} can be one of these values: {1}").\
                format(opt_name, POSS_VALUES[opt_name])
            raise Exception(error)
        arguments[opt_name] = opt_value
    return arguments

def _check_options_match_conf(options, full_conf):
    """Check carrier and JMS provided (if any) are all defined in 
    the configuration file.
    """
    carriers_conf = full_conf['carriers']
    carriers = carriers_conf.keys()
    jmss_conf = full_conf['jmss']
    jmss = jmss_conf.keys() 
    # check carrier
    opt_carrier = options['carrier']
    if opt_carrier:
        if opt_carrier not in carriers:
            raise Exception(("Carrier '{0}' is not defined in the conf file.").\
                format(opt_carrier))
        if not carriers_conf[opt_carrier]:
            raise Exception(("No JMS was defined for carrier '{0}'.").\
                format(opt_carrier))
        for jms in carriers_conf[opt_carrier]:
            if jms not in jmss:
                raise Exception(("JMS '{0}' was listed under carrier '{1}'" + 
                " but it was not defined on config file.").format(
                    jms, opt_carrier))
    # check jms
    if '-j' in options:
        opt_jms = options['-j']
        if opt_jms not in jmss:
            raise Exception(("JMS '{0}' is not defined in the config file.").\
                format(options['-j']))
        if opt_carrier and opt_jms not in carriers_conf[opt_carrier]:
            raise Exception(("JMS '{0}' was not listed under carrier '{1}'.").\
                format(opt_jms, opt_carrier))

def _get_jmss(options, full_conf):
    """Return the list of JMSs to fetch the data from."""
    if '-j' in options:
        jmss = [options['-j']]
    elif options['carrier']:
        carrier = options['carrier']
        # convert to set to guarantee JMS servers are not repeated
        jmss = list(set(full_conf['carriers'][carrier]))
    else:  # all JMSs
        jmss = full_conf['jmss'].keys()
    return sorted(jmss)

def _safe_action(jms, data):
    """Thread safe for adding data to the global dict result."""
    with s_action_lock:
        jms_data[jms] = data

def _create_file2execute(jms, current_time):
    """Create the temporal script to be executed for interacting with WSLT.
    Return the file name (complete path).
    """
    _file = None
    tmp_exe_file = REMOTE_EXE_FILE_TEMP.format(jms_name=jms, 
                                               current_time=current_time)
    try:
        _file = open(tmp_exe_file, 'w+')
    except Exception as e:
        raise Exception(str(e))
    finally:
        if _file:
            _file.close()
    return tmp_exe_file

def _add_content2file(file_path, jms, jms_conf, jms_credentials_conf, 
                      json_result_file):
    """Add needed content dynamicaly to the remote file to be executed in 
    the JMS.

    Keyword arguments:
    file_path -- complete path to the file to be executed remotely in the JMS, 
        and the one the content is being added to.
    jms -- name of the JMS server the file is going to be executed in.
    jms_conf -- JSON configuration for the JMS. It has this form:
        {
            <jms>: { 
                "port": <port2connect2jms_server>,
                "wlst_queue_bean": <specific_string_for_wlst_exec>,
                "normal_queue": True | False
            }
            "defaults": { 
                "port": <port2connect2jms_server>,
                "wlst_queue_bean": <specific_string_for_wlst_exec>,
                "normal_queue": True | False
            }
        }
        Either <jms> or "defaults" must be specified.
    jms_credentials_conf -- JSON holding the credentials to be used withing 
        this JMS with WLST. It has this form:
        {
            "user": <username>,
            "pass": <password>
        }
    json_result_file -- name (without the path) of the JSON file to be used 
        remotely for saving the output of checking the JMS.
    """
    credentials_txt = "credentials = {0}".format(jms_credentials_conf)
    jms_txt = "jms_server = '{0}'".format(jms)
    json_result_txt = "json_result = '{0}'".format(json_result_file)
    # data needed to use the WLST
    wlst_port = jms_conf[jms]['port'] \
                        if 'port' in jms_conf[jms] \
                        else jms_conf['defaults']['port']
    wlst_bean_normal_queue = jms_conf[jms]['wlst_bean_normal_queue'] \
                        if 'wlst_bean_normal_queue' in jms_conf[jms] \
                        else jms_conf['defaults']['wlst_bean_normal_queue']
    wlst_bean_dist_queue = jms_conf[jms]['wlst_bean_dist_queue'] \
                            if 'wlst_bean_dist_queue' in jms_conf[jms] \
                            else jms_conf['defaults']['wlst_bean_dist_queue']
    wlst_port_txt = "wlst_port = '{0}'".format(wlst_port)
    wlst_bean_normal_queue_txt = "wlst_bean_normal_queue = '{0}'".format(
                                                        wlst_bean_normal_queue)
    wlst_bean_dist_queue_txt = "wlst_bean_dist_queue = '{0}'".format(
                                                        wlst_bean_dist_queue)
    dynamic_content = "\n".join([
        credentials_txt, jms_txt, json_result_txt, wlst_port_txt, 
        wlst_bean_normal_queue_txt, wlst_bean_dist_queue_txt
    ])
    with open(REMOTE_EXE_FILE) as do_not_edit_file:
        with open(file_path, 'w') as executable_rmt_file:
            executable_rmt_file.write(dynamic_content)
            for line in do_not_edit_file:
                executable_rmt_file.write(line)

def _check_credentials_exist(jmss, credentials_conf):
    """Check the credentials exist for the passed JMS.
    Raise an exception if credentials don't exist.
    """
    for jms in jmss:
        if jms not in credentials_conf or \
           'user' not in credentials_conf[jms] or \
           'pass' not in credentials_conf[jms]:
            raise Exception(("No credentials were defined for JMS {0}.").\
                format(jms))

def _fetch_data(jms, rmt_file2exe, jms_conf, json_result_file):
    """Fetch data from JMS.

    Keyword arguments:
    jms -- JMS to fetch data from.
    rmt_file2exe -- complete path to the file to execute remotely in the JMS.
    jms_conf -- JSON configuration for the JMS. It has this form:
        {
            <jms>: { 
                "username": <username2connect2jms_server>,
                "wlst_script": <script2set_environment_in_jms>
            }
            "defaults": { 
                "username": <username2connect2jms_server>,
                "wlst_script": <script2set_environment_in_jms>
            }
        }
        Either <jms> or "defaults" must be specified.
    json_result_file -- name of the JSON file (without the path) to be used 
        for saving remotely the output of checking this JMS.

    Return a dictionary of this form:
        {
            "error": <error_message_if_any>,
            "queues": {
                "<queue_name>": {
                    "consumers_count": <consumers_count>,
                    "current_messages_count": <current_messages_count>,
                    "pending_messages_count": <pending_messages_count>

                },
                "..."
            }
        }
    """
    username = jms_conf[jms]['username'] \
                        if 'username' in jms_conf[jms] \
                        else jms_conf['defaults']['username']
    env_script = jms_conf[jms]['wlst_script'] \
                        if 'wlst_script' in jms_conf[jms] \
                        else jms_conf['defaults']['wlst_script']
    python_script_name = path.basename(rmt_file2exe)
    command = COMMAND.format(script=rmt_file2exe, user=username, jms=jms, 
                             set_env_script=env_script, 
                             rmt_script=python_script_name, 
                             json_result=json_result_file)
    p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, 
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate()
    if error.strip():
        logger.error(error.strip())
    return ast.literal_eval(output.strip())

def _sort_data(jms):
    """Sort data by columns 'Current Messages' and 'Pending Messages' in 
    ascending order.
    """
    sorted_data = []
    inserted = False
    for queue in jms_data[jms]['queues']:
        current_queue_dict = jms_data[jms]['queues'][queue]
        for index, sorted_d in enumerate(sorted_data):
            sorted_d_key_name = sorted_d.keys()[0]
            if int(current_queue_dict['current_messages']) < \
                    int(sorted_d[sorted_d_key_name]['current_messages']) or \
                    (int(current_queue_dict['current_messages']) == \
                    int(sorted_d[sorted_d_key_name]['current_messages']) and \
                    int(current_queue_dict['pending_messages']) < \
                    int(sorted_d[sorted_d_key_name]['pending_messages'])):
                sorted_data.insert(index, {queue: current_queue_dict})
                inserted = True
                break
        if not inserted:
            sorted_data.append({queue: current_queue_dict})
        inserted = False
    return sorted_data

def _print_tabled_data(html=False, sort_data=False):
    """Print the result coming from the JMS in the terminal.
    Output is printed in a table, either in ascending order by queue name, or 
    by column 'Current Messages' in ascending order if sort_data is True.
    """
    headers = ['Queue Name', 'Consumers', 'Current Messages', 
               'Pending messages']
    for jms in jms_data:
        data = []
        if not html:
            print("\n\t{0}\n---------------------------\n".format(jms.upper()))
        if sort_data:
            queues = _sort_data(jms)
            for queue in queues:
                _q = queue.keys()[0]
                consumers = jms_data[jms]['queues'][_q]['consumers']
                p_messages = jms_data[jms]['queues'][_q]['pending_messages']
                c_messages = jms_data[jms]['queues'][_q]['current_messages']
                data.append([_q, consumers, c_messages, p_messages])
        else:
            queues = sorted(jms_data[jms]['queues'].keys())
            for queue in queues:
                consumers = jms_data[jms]['queues'][queue]['consumers']
                p_messages = jms_data[jms]['queues'][queue]['pending_messages']
                c_messages = jms_data[jms]['queues'][queue]['current_messages']
                data.append([queue, consumers, c_messages, p_messages])
        print(util.tabled_data(data, headers) if not html else \
            util.tabled_html_data(data, headers, enablesort=True))

def _notify(message_body, email_conf):
    """Send the alert email and keep log of that."""
    if message_body:
        logger.info("Trying to trigger alert email...")
        dests = email_conf['dest']
        if dests:
            email_body = email_conf['body'].format(body=''.join(message_body))
            result = emailer.send_email(
                subject=email_conf['subject'],
                body=email_body,
                dest=dests,
                html=True,
                highpriority=True
            )
            if result == 'OK':
                logger.info("Email was sent to {0}".format(', '.join(dests)))
            else:
                logger.error(("Message couldn't be sent to {0} because of " + 
                            "error: {1}").format(', '.join(dests), result))
        else:
            logger.warning("No email addresses were defined to send the " + 
                           "alert to.")
    else:
        logger.info("Nothing to notify.")

def _alert_purpose(full_conf):
    """Create the message and send it by email if at least one queue reached 
    or surpassed the threshold defined in full_conf.
    """
    logger.info("Checking for alerts...")
    message_body = []
    jms_name_css = "<h4>{jms}</h4>"
    headers = ['Queue Name', 'Consumers', 'Current Messages', 
               'Pending messages', 'Threshold']
    default_threshold = full_conf['settings']['defaults']['threshold']
    for jms in jms_data:
        jms_added = False
        conf_jms = full_conf['jmss'][jms]
        warning_queues = []
        for queue in jms_data[jms]['queues']:
            cur_mess = int(jms_data[jms]['queues'][queue]['current_messages'])
            threshold = conf_jms['q_thresholds'][queue] if 'q_thresholds' \
                in conf_jms and queue in conf_jms['q_thresholds'] \
                else default_threshold
            if cur_mess >= threshold:
                if not jms_added:
                    message_body.append(jms_name_css.format(jms=jms.upper()))
                    jms_added = True
                warning_queues.append([
                    queue, 
                    int(jms_data[jms]['queues'][queue]['consumers']),
                    int(jms_data[jms]['queues'][queue]['current_messages']),
                    int(jms_data[jms]['queues'][queue]['pending_messages']),
                    threshold
                ])
                logger.warning(
                            ("Queue '{0}' in {1} reached or surpassed the " + 
                                "threshold of {2}. Currently it's at {3}").\
                                format(queue, jms, threshold, cur_mess))
        if warning_queues:
            message_body.append(util.tabled_html_data(warning_queues, headers))
    _notify(message_body, full_conf['settings']['email'])

def _consume_data(options, full_conf):
    """Decide what to do with the data coming from every JMS, which is on 
    global dictionary 'jms_data'.
    """
    if '-p' in options and options['-p'] == 'alert':
        _alert_purpose(full_conf)
    else:
        sort_data = '-s' in options and options['-s'] == 'count'
        html = '-p' in options and options['-p'] == 'web'
        _print_tabled_data(html=html, sort_data=sort_data)

def _check_jms(jms, current_time, jms_conf, jms_cred):
    """Prepare stuff and connect and execute the temporal script into JMS.

    Keyword arguments:
    jms -- name of the JMS server.
    current_time -- current time in seconds since EPOCH. Used as part of the 
        name of the temporal script, so it's unique.
    jms_conf -- JSON configuration for this JMS.
    jms_cred -- JSON configuration holding the credentials for this JMS.

    This function updates the global variable 'jms_data' with the data 
    coming from checking this JMS.
    """
    logger.info("Checking JMS {0}".format(jms))
    try:
        rmt_file2exe = _create_file2execute(jms, current_time)
        # create JSON file with the same name of the Python script
        json_result_file = path.basename(rmt_file2exe).replace('.py', '.json')
        _add_content2file(rmt_file2exe, jms, jms_conf, jms_cred, 
                            json_result_file)
        data = _fetch_data(jms, rmt_file2exe, jms_conf, json_result_file)
        _safe_action(jms, data)
        logger.info("Finished checking JMS {0}".format(jms))
    finally:
        if rmt_file2exe and path.isfile(rmt_file2exe):
            system("rm -f {0}".format(rmt_file2exe))
            if path.isfile(rmt_file2exe):
                logger.warning("File {0} couldn't be removed.".format(
                                                                rmt_file2exe))

def _calm_user(options):
    """Ask the user to be patient while the data is being fetched.
    Only to be used in terminal (-p is not coming in script params).
    """
    if '-p' not in options:
        print("Fetching data... please be patient")
        print("We are dealing here with WLST...")

def _main():
    full_params = sys.argv
    try:
        _init(full_params)
        options = _check_params(full_params)
        full_conf = conf.get_conf(CONF_FILE)
        _check_options_match_conf(options, full_conf)
        credentials_conf = conf.get_conf(CREDENTIALS_FILE)
        jmss = _get_jmss(options, full_conf)
        _check_credentials_exist(jmss, credentials_conf)
        _calm_user(options)
        pool = Pool(len(jmss))
        current_time = time.time()
        for jms in jmss:
            jms_conf = {}
            jms_conf[jms] = full_conf['jmss'][jms]
            jms_conf['defaults'] = full_conf['settings']['defaults']
            jms_cred = credentials_conf[jms]
            pool.apply_async(_check_jms, 
                            (jms, current_time, jms_conf, jms_cred))
        pool.close()
        pool.join()
        _consume_data(options, full_conf)
    except Exception as e:
        logger.critical(str(e))
    logger.info("I finished!")

if __name__ == "__main__":
    _main()
