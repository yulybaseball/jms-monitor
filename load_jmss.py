#!/bin/env python

"""
This scripts is the one called from the front end to load JMSs 
defined in the conf file.

It prints back the list of JMSs.
"""

import json
from os import path
import sys

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

def _print2frontend(data):
    print(json.dumps(data))

try:
    from pssapi.utils import conf

    EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
    CONF_FILE = path.join(EXEC_DIR, 'conf', 'conf.json')

    full_conf = conf.get_conf(CONF_FILE)
    _print2frontend({"jmss": sorted(full_conf['jmss'].keys())})
except Exception as e:
    _print2frontend({"error": str(e)})
