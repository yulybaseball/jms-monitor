


"""
IMPORTANT!!

This script is not intended to be run by itself. This is a complement for 
an application that checks queue data on JMS servers.

This basically connects to WLST and perform the fetching of queue data.
"""

response = {
    'error': "",
    'queues': {}
}
uname = credentials['user']
passw = credentials['pass']
url = 't3://' + jms_server + ':' + wlst_port
try:
    connect(uname, passw, url)
    allJMSResources = cmo.getJMSSystemResources()
    for jmsResource in allJMSResources:
        # normal queues
        normalQueues = jmsResource.getJMSResource().getQueues()
        serverRuntime()
        for oneNormalQueue in normalQueues:
            normalname = oneNormalQueue.getJNDIName()
            normaljms = getMBean(wlst_bean_normal_queue + normalname)
            if normaljms is not None:
                response['queues'][normalname] = {
                    'consumers': str(normaljms.getConsumersCurrentCount()),
                    'current_messages': str(normaljms.getMessagesCurrentCount()),
                    'pending_messages': str(normaljms.getMessagesPendingCount())
                }
        # dist queues
        uniformDistributedQueues = jmsResource.getJMSResource().getUniformDistributedQueues()
        # serverRuntime()
        for oneQueue in uniformDistributedQueues:
            name = oneQueue.getName()
            jms = getMBean(wlst_bean_dist_queue + name)
            if jms is not None:
                response['queues'][name] = {
                    'consumers': str(jms.getConsumersCurrentCount()),
                    'current_messages': str(jms.getMessagesCurrentCount()),
                    'pending_messages': str(jms.getMessagesPendingCount())
                }
    disconnect()
except Exception, e:
    response['error'] = str(e)

try:
    _f = file(json_result, 'w')
    _f.write(str(response))
    _f.close()
except IOError, e:
    pass
